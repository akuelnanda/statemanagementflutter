class LeaveDataModel {
  String usedLeave;
  String onProgressLeave;
  String remainingLeave;

  LeaveDataModel(
      {required this.usedLeave,
      required this.onProgressLeave,
      required this.remainingLeave});
}
