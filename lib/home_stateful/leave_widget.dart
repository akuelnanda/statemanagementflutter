import 'package:flutter/material.dart';
import 'package:state_sample_project/model/leave_data_model.dart';
import 'package:state_sample_project/repositories/home_repository.dart';

class LeaveWidget extends StatefulWidget {
  @override
  _LeaveWidgetState createState() => _LeaveWidgetState();
}

class _LeaveWidgetState extends State<LeaveWidget> {
  late LeaveDataModel leaveData;
  late HomeRepository homeRepository = HomeRepository();

  @override
  void initState() {
    super.initState();
    leaveData = homeRepository.getDataLeave();
  }

  @override
  Widget build(BuildContext context) {
    print("Build ulang Leave dijalankan");
    return Card(
      elevation: 5, // Controls the shadow depth
      margin: EdgeInsets.all(16), // Controls the spacing around the card
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.album),
            title: Text('Data Cuti'),
          ),
          Padding(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'Used Leave : ' + leaveData!.usedLeave + ' ',
              style: TextStyle(fontSize: 16),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'On Progress Leave : ' + leaveData!.onProgressLeave + ' ',
              style: TextStyle(fontSize: 16),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'Remaining Leave :' + leaveData!.remainingLeave + ' ',
              style: TextStyle(fontSize: 16),
            ),
          ),
          ButtonBar(
            children: <Widget>[
              TextButton(
                onPressed: () {
                  setState(() {
                    leaveData = LeaveDataModel(
                        usedLeave: "4",
                        onProgressLeave: "4",
                        remainingLeave: "4");
                  });
                },
                child: Text('Refresh Cuti'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
