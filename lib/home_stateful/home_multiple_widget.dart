import 'package:flutter/material.dart';
import 'package:state_sample_project/home_stateful/absensi_widget.dart';
import 'package:state_sample_project/home_stateful/employee_widget.dart';
import 'package:state_sample_project/home_stateful/leave_widget.dart';

class HomeMultiplelWidget extends StatefulWidget {
  @override
  _HomeMultipleWidgetState createState() => _HomeMultipleWidgetState();
}

class _HomeMultipleWidgetState extends State<HomeMultiplelWidget> {
  @override
  Widget build(BuildContext context) {
    print("Build ulang Home Dijalankan");
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home Page Example 1'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [EmployeeWidget(), AbsensiWidget(), LeaveWidget()],
        ),
      ),
    );
  }
}
