import 'package:flutter/material.dart';
import 'package:state_sample_project/model/employee_data_model.dart';
import 'package:state_sample_project/repositories/home_repository.dart';

class EmployeeWidget extends StatefulWidget {
  @override
  _EmployeeWidgetState createState() => _EmployeeWidgetState();
}

class _EmployeeWidgetState extends State<EmployeeWidget> {
  late List<EmployeeDataModel> employeeList = [];
  late HomeRepository homeRepository = HomeRepository();
  var itemCountEmployeeList = 0;

  @override
  void initState() {
    super.initState();
    employeeList = homeRepository.getDataEmployeeData();
    itemCountEmployeeList = employeeList.length;
  }

  @override
  Widget build(BuildContext context) {
    print("Build ulang dijalankan Employee");
    return Card(
      elevation: 5, // Controls the shadow depth
      margin: EdgeInsets.all(16), // Controls the spacing around the card
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            leading: Icon(Icons.album),
            title: Text('Employee Data'),
          ),
          SizedBox(
            height: 300,
            child: ListView.builder(
                itemCount: itemCountEmployeeList,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                      leading: const Icon(Icons.list),
                      title: Text(employeeList[index].name));
                }),
          ),
          ButtonBar(
            children: <Widget>[
              TextButton(
                onPressed: () {
                  setState(() {
                    itemCountEmployeeList++;
                    employeeList.add(EmployeeDataModel(
                        name: "Employee " + itemCountEmployeeList.toString(),
                        alamat: "Bogor"));
                  });
                },
                child: Text('Add Employee'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
