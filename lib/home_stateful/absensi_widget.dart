import 'package:flutter/material.dart';
import 'package:state_sample_project/model/absen_data_model.dart';
import 'package:state_sample_project/model/leave_data_model.dart';
import 'package:state_sample_project/model/employee_data_model.dart';
import 'package:state_sample_project/repositories/home_repository.dart';

class AbsensiWidget extends StatefulWidget {
  @override
  _AbsensiWidgetState createState() => _AbsensiWidgetState();
}

class _AbsensiWidgetState extends State<AbsensiWidget> {
  late AbsensiDataModel? absensiData;
  late HomeRepository homeRepository = HomeRepository();

  @override
  void initState() {
    super.initState();
    absensiData = homeRepository.getDataAbsensi();
  }

  @override
  Widget build(BuildContext context) {
    print("Build ulang Absensi Dijalankan");
    return Card(
      elevation: 5, // Controls the shadow depth
      margin: EdgeInsets.all(20), // Controls the spacing around the card
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            leading: Icon(Icons.album),
            title: Text('Data Absensi'),
            subtitle: Text('Today Attendance Still Missing'),
          ),
          Padding(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'Checkin : ' + absensiData!.checkin + ' ',
              style: TextStyle(fontSize: 16),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'Checkout : ' + absensiData!.checkout + ' ',
              style: TextStyle(fontSize: 16),
            ),
          ),
          ButtonBar(
            children: <Widget>[
              TextButton(
                onPressed: () {
                  setState(() {
                    absensiData = absensiData = AbsensiDataModel(
                        checkin: "(09:00)", checkout: "Missing");
                  });
                  // Add your action here
                },
                child: Text('Update Data'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
