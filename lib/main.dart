import 'package:flutter/material.dart';
import 'package:state_sample_project/home_stateful/home_multiple_widget.dart';
import 'package:state_sample_project/home_stateful/home_single_widget.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      //home: HomeSingleWidget(),
      home: HomeMultiplelWidget(),
    );
  }
}
