import 'package:state_sample_project/model/absen_data_model.dart';
import 'package:state_sample_project/model/leave_data_model.dart';
import 'package:state_sample_project/model/employee_data_model.dart';

class HomeRepository {
  List<EmployeeDataModel> getDataEmployeeData() {
    List<EmployeeDataModel> results = [];
    results
        .add(EmployeeDataModel(name: "Mr Ahmad Dani", alamat: "Jakarta Pusat"));
    results.add(EmployeeDataModel(name: "Mr Once", alamat: "Jakarta Selatam"));
    return results;
  }

  LeaveDataModel getDataLeave() {
    LeaveDataModel leave = LeaveDataModel(
        usedLeave: "2", onProgressLeave: "2", remainingLeave: "8");
    return leave;
  }

  AbsensiDataModel getDataAbsensi() {
    AbsensiDataModel absensi = AbsensiDataModel(checkin: "08:00", checkout: "");
    return absensi;
  }
}
